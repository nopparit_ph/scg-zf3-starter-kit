<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
require 'PHPMailer/PHPMailerAutoload.php';
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class XxxController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'index');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);
        return $view;       
    } 
################################################################################
    public function indexAction() 
    {
        try
        {
            //echo "xx";
            $view = new ViewModel();
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
    public function sendAction() 
    {
        try
        {
            $mail             = new PHPMailer();

            //$mail->IsSMTP(); // telling the class to use SMTP
            //$mail->Host       = "mail.yourdomain.com"; // SMTP server
            //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "tls";                 
            $mail->Host       = "smtp.gmail.com";      // SMTP server
            $mail->Port       = 587;                   // SMTP port
            $mail->Username   = "codeismagicmailer@gmail.com";  // username
            $mail->Password   = "C0d31sm9ic!";            // password

            $mail->SetFrom('losociety@hotmail.com', 'Nopparit Phuwaketanon');

            $mail->Subject    = "RE - SCG - Assignment for Software Developer Position";

            $mail->MsgHTML('Dear SCG <br/> this email send by test program for Assignment for Software Developer Position <br/> My Giy url : https://bitbucket.org/nopparit_ph/scg-zf3-starter-kit.git <br/> my bitbucket url : https://bitbucket.org/nopparit_ph <br/> Best Regards, <br/> Nopparit Ph. <br/> Software Developer Position Candidate <br/> 0896197557');

            $mail->AddAddress("Tonywilk@scg.com", "Line Manager");
            $mail->AddAddress("losociety@hotmail.com", "Nopparit");

            if(!$mail->Send()) {
              echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
              echo "Message sent!";
            }

            return new ViewModel();
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
}